package com.lwise.javabasic01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasic01Application {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasic01Application.class, args);
    }

}
